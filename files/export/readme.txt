IMPORT AN EXPORTED MODULE

The zip file contains:

- model folder:
    This folder contains a yaml file which needs to be copied to files/model/app.yaml.
    Then, the app model must be generated.

- php classes:
    Those classes need to replace the ones in Own\App\{module_name}\

- template folder:
    This folder need to replace the existing template folder in Own\App\{module_name}\

- script folder:
    This folder contains a sql file which needs to be run on the database.
