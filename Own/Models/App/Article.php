<?php
/**
* (c) Vincent Patry
* This file is part of the Rebond package
* For the full copyright and license information, please view the LICENSE.txt
* file that was distributed with this source code.
*/
namespace Own\Models\App;

class Article extends \Generated\Models\App\BaseArticle
{
    public function __construct()
    {
        parent::__construct();
        $this->setAppDefault();
    }

    public function setAppDefault()
    {
        $this->setAppDefaultBase();
        // set your defaults values here
    }
}
