<?php
/**
 * (c) Vincent Patry
 * This file is part of the Rebond package
 * For the full copyright and license information, please view the LICENSE.txt
 * file that was distributed with this source code.
 */

namespace Own\Gadgets\App;

use Rebond\App;
use Rebond\Forms\Core\UserForm;
use Rebond\Gadgets\AbstractGadget;
use Rebond\Models\Core\User;
use Rebond\Services\Auth;
use Rebond\Services\Converter;
use Rebond\Services\Core\UserService;
use Rebond\Services\Form;
use Rebond\Services\Session;
use Rebond\Services\Template;

class UserGadget extends AbstractGadget
{
    public function __construct(App $app)
    {
        parent::__construct($app, 'User');
    }

    public function status()
    {
        $signedUser = $this->app->getUser();
        $tpl = new Template(Template::MODULE, ['app', 'user']);

        if (Auth::isAuth($signedUser)) {
            $tpl->set('fullName', $signedUser->getFullName());
        }
        return $tpl->render('status');
    }

    public function signIn()
    {
        $signedUser = $this->app->getUser();

        // action
        $userForm = new UserForm($signedUser);
        $userForm->signIn();

        if (Auth::isAuthorized($userForm->getModel(), 'member')) {
            Session::redirect('/');
        }

        $tpl = new Template(Template::MODULE, ['app', 'user']);
        $tpl->set('item', $userForm);
        return $tpl->render('sign-in');
    }

    public function forgotPassword()
    {
        $email = Converter::stringKey('email', 'post');
        $resetKey = Converter::stringKey('reset');
        return UserService::forgotPassword(
            $this->app,
            Form::isSubmitted('btnSend'),
            $email,
            $resetKey,
            Form::isSubmitted('btnPasswordReset')
        );
    }

    public function register()
    {
        // auth
        $signedUser = $this->app->getUser();

        if (Auth::isAuth($signedUser)) {
            Session::redirect('/profile');
        }

        $tpl = new Template(Template::MODULE, ['app', 'user']);

        $user = new User();
        $userForm = new UserForm($user);

        if (Form::isSubmitted('btnRegister')) {
            if (UserService::createOrEdit($userForm, true, true)) {
                return $tpl->render('register-success');
            }
        }

        // confirm email
        $confirm = Converter::stringKey('confirm');
        if (isset($confirm)) {
            $tplConfirm = UserService::confirmUser($userForm, $confirm, Form::isSubmitted('btnPasswordCreate'));
            if (isset($tplConfirm)) {
                return $tplConfirm;
            }
        }

        $tpl->set('item', $userForm);
        return $tpl->render('register');
    }

    public function profile()
    {
        $signedUser = $this->app->getUser();

        // auth
        if (!Auth::isAuth($signedUser)) {
            Session::redirect('/profile/sign-in');
        }

        $userForm = new UserForm($signedUser);

        if (Form::isSubmitted()) {
            if (UserService::createOrEdit($userForm, false, false)) {
                Session::allSuccess('saved', '/profile');
            }
        }

        $tpl = new Template(Template::MODULE, ['app', 'user']);
        $tpl->set('item', $userForm);

        return $tpl->render('profile');
    }

    public function changePassword()
    {
        $signedUser = $this->app->getUser();

        // auth
        if (!Auth::isAuth($signedUser)) {
            Session::redirect('/profile/sign-in');
        }

        $userForm = new UserForm($signedUser);

        // action
        if (Form::isSubmitted('btnPasswordReset')) {
            if ($userForm->changePassword(false, true)) {
                Session::allSuccess('password_changed', '/profile');
            }
        }

        $tpl = new Template(Template::MODULE, ['app', 'user']);
        $tpl->set('action', 'password_change');
        $tpl->set('item', $userForm);
        $tpl->set('checkCurrentPassword', true);
        return $tpl->render('password-change');
    }

    public function signOut()
    {
        UserService::signOut($this->app->getUser());
    }
}