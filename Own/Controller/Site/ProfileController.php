<?php
namespace Own\Controller\Site;

use Own\Gadgets\App\UserGadget;
use Rebond\Services\Converter;
use Rebond\Services\Core\UserService;
use Rebond\Services\Form;
use Rebond\Services\Lang;

class ProfileController extends BaseController
{
    public function setTpl()
    {
        parent::setBaseTpl();
        $this->tplMaster->set('title', Lang::lang('profile'));
    }

    public function index()
    {
        $userGadget = new UserGadget($this->app);
        $status = $userGadget->status();

        // view
        $this->setTpl();

        // layout
        $this->tplLayout->set('column1', $status);

        // template
        $this->tplMaster->set('layout', $this->tplLayout->render('layout-1-col'));
        return $this->tplMaster->render('tpl-default');
    }

    public function signIn()
    {
        $userGadget = new UserGadget($this->app);
        $signInTpl = $userGadget->signIn();

        // view
        $this->setTpl();

        // layout
        $this->tplLayout->set('column1', $signInTpl);

        // template
        $this->tplMaster->set('layout', $this->tplLayout->render('layout-2-col'));
        return $this->tplMaster->render('tpl-default');
    }

    public function forgotPassword()
    {
        $email = Converter::stringKey('email', 'post');
        $resetKey = Converter::stringKey('reset');
        $tplForgot = UserService::forgotPassword(
            $this->app,
            Form::isSubmitted('btnSend'),
            $email,
            $resetKey,
            Form::isSubmitted('btnPasswordReset')
        );

        // view
        $this->setTpl();

        // layout
        $this->tplLayout->set('column1', $tplForgot);

        // template
        $this->tplMaster->set('layout', $this->tplLayout->render('layout-2-col'));

        return $this->tplMaster->render('tpl-default');
    }

    public function register()
    {
        $userGadget = new UserGadget($this->app);
        $registerTpl = $userGadget->register();

        // view
        $this->setTpl();

        // layout
        $this->tplLayout->set('column1', $registerTpl);

        // template
        $this->tplMaster->set('layout', $this->tplLayout->render('layout-2-col'));

        return $this->tplMaster->render('tpl-default');
    }

    public function profile()
    {
        $userGadget = new UserGadget($this->app);
        $profileTpl = $userGadget->profile();

        // view
        $this->setTpl();

        // layout
        $this->tplLayout->set('column1', $profileTpl);

        // template
        $this->tplMaster->set('layout', $this->tplLayout->render('layout-1-col'));
        return $this->tplMaster->render('tpl-default');
    }

    public function changePassword()
    {
        $userGadget = new UserGadget($this->app);
        $changePasswordTpl = $userGadget->changePassword();

        // view
        $this->setTpl();

        // layout
        $this->tplLayout->set('column1', $changePasswordTpl);

        // template
        $this->tplMaster->set('layout', $this->tplLayout->render('layout-2-col'));

        return $this->tplMaster->render('tpl-default');
    }

    public function signOut()
    {
        UserService::signOut($this->app->getUser());
    }
}
