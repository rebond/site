<?php
namespace Own\Controller\Admin;

use Rebond\Controller\Admin\BaseAdminController;
use Rebond\Services\Auth;
use Rebond\Services\Lang;
use Rebond\Services\Template;

class OwnController extends BaseAdminController
{
    public function index()
    {
        Auth::isAdminAuthorized($this->signedUser, 'admin.own', true, '/');

        $tplDefault = new Template(Template::SITE, ['admin']);

        return $this->response(
            'tpl-default', [
                'title' => Lang::lang('own'),
                'jsLauncher' => 'own',
            ],
            'layout-1-col', [
                'column1' => $tplDefault->render('index')
            ]
        );
    }
}
