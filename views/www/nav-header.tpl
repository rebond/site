<div id="rb-nav">
    <div class="nav">
        <div class="logo">
            <h1><?php echo $title ?></h1>
            <a href="/"><img alt="rebond" src="/images/brand-icon.png" /></a>
        </div>
        <?php echo $nav ?>
    </div>
</div>

<a href="#" id="rb-burger">
    <span></span>
    <span class="mid"></span>
    <span></span>
</a>