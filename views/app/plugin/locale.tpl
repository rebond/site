<div class="bg-white">
<?php echo $this->lang('lang_change') ?>:
<?php if (isset($langs)) {
    $first = true;
    foreach ($langs as $key => $locale) {
        if ($first) {
            $first = false;
        } else {
            echo ' | ';
        }
        echo '<a href="?lang=' . $key . '">' . $key . '</a>';
    }
} ?>
</div>
<div class="clear"></div>