<div class="bg-white">
    <h2><?php echo $item->getTitle() ?>
        <?php if ($filter) { ?>
        <small><?php echo $item->getFilter() ?></small>
        <?php } ?>
    </h2>
    <img src="<?php echo $this->showFromModel($item->getMedia()) ?>" alt="<?php echo $item->getMedia()->getTitle() ?>" /><br>
<?php echo $item->getDescription() ?><br>
</div>