<div class="bg-white">
    <h2><?php echo $title ?></h2>
    <p><?php echo $error ?></p>
    <p>
        <a href="<?php echo $currentUrl ?>"><?php echo $this->lang('reload') ?></a> |
        <a href="/"><?php echo $this->lang('back_home') ?></a>
    </p>
</div>
